#React Boilerplate without a Flux framework#

###Left node_modules folder in repo on purpose just so you do not have to re-download them.If you do want or need to install them all again use###

`````
$ npm install
`````

###may also need to install webpack globally this project also requires node.js to be install###

`````
$ npm install webpack -g
`````

`````
$ npm install webpack-dev-server
`````

##To Build Project##
`````
$ webpack
`````

##To Run and Build Project##
`````
$ webpack-dev-server
`````

## Navigate to##
`````
localhost:8080
`````

### *Flux is not needed for a React project Flux is just a data flow construct that was made in tandem with React* ###

### This does have a react immutablity helper which is not required but is best practice and it also has react-router which is not needed for the most basic react example but I wanted to give a very basic example of how that works as well ###


