import React from 'react';
import {Route, IndexRoute } from 'react-router';
import Main from './src/Containers/Main.jsx';
import Home from './src/Containers/Home.jsx';
import HelloWorld from './src/Containers/HelloWorld.jsx';

export default (
  <Route path="/" component={Main}>
    <IndexRoute component={Home} />
    <Route path="helloworld" component={HelloWorld}/>
  </Route>
);