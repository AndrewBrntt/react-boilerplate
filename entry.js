import React from 'react';
import { render } from 'react-dom';
import { RouterContext, Router, hashHistory } from 'react-router';
import Main from './src/Containers/Main.jsx';
import routes from './routes.js';

render((
  <Router history={hashHistory} render={props => <RouterContext {...props}/>}>
    {routes}
  </Router>
), document.getElementById('main'));