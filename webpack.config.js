/**
 * Created by Andrew on 10/28/2015.
 */
'use strict';

module.exports = {
    entry: "./entry.js",
    output: {
        path: __dirname + '/build',
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader"}
        ]
    },
    devtool: '#inline-source-map'
};