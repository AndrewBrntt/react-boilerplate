import React from 'react';

export default class HelloWorldComponent extends React.Component {

  render () {
    return (<div>
      {this.props.differentPlanets}
      <button onClick={this.props.addNewPlanet.bind(this)}> New Planet</button>
    </div>)
  }
}