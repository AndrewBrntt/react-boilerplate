import React from 'react';
import update from 'react-addons-update';
import HelloWorldComponent from '../Components/HelloWorldComponent.jsx'

export default class HelloWorld extends React.Component {

  constructor () {
    super();

    //this is the only place you are allowed to directly manipulate state
    this.state = {
      planets: ["Mercury", "Venus", "Earth"]
    };
  }

  addNewPlanet () {

    console.log(update);
    var planetName = "Planet Bob";
    //this is so state is no longer being updated directly using react immutablity addons
    var newPlanetList = update(this.state, {
      planets: {$push: [planetName]}
    });

    this.setState(newPlanetList);

  }

  render () {
    console.log(this.state.planets);
    return (<HelloWorldComponent addNewPlanet={this.addNewPlanet.bind(this)} differentPlanets={this.state.planets}/>)

  }

}