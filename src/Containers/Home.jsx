import React from 'react';
import {Router} from 'react-router';
import HomeComponent from '../Components/HomeComponent.jsx';

export default class Home extends React.Component {

  constructor (props,context) {
    super(props);

    context.router;
  }

  gotoHelloWorld () {
    this.context.router.push('/helloworld');
  }

  render () {

    return (<HomeComponent gotoHelloWorld={this.gotoHelloWorld.bind(this)}/>)

  }

}

Home.contextTypes = {
  router: React.PropTypes.object.isRequired
};
